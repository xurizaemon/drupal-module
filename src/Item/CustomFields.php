<?php

namespace Drupal\archimedes_client\Item;

use Drupal\archimedes_client\Item;
use Drupal\Core\Site\Settings;

/**
 * Custom Fields.
 *
 * Custom Fields pre defined by the user.
 *
 * @package Archimedes
 * @subpackage Client
 */
class CustomFields extends Item {

  /**
   * Gets an array of custom fields, keyed numerically.
   *
   * @return array
   *   custom fields
   */
  public function get() {

    $field_handler = \Drupal::config('archimedes_client.settings')->get('settings.fields');

    $field_custom = Settings::getAll();

    $parts = [];
    $parts = explode(',', $field_handler);
    $fields = [];

    foreach ($parts as $name => $field) {

      $fields[] = [
        trim($parts[$name]) => Settings::get(trim($parts[$name])),
      ];
    }
    return $fields;
  }

  /**
   * Gets a string denoting the Custom Fields.
   *
   * @return string
   *   render customs fields
   */
  public function render() {

    $field_handler = \Drupal::config('archimedes_client.settings')->get('settings.fields');

    $parts = [];
    $fields = " ";

    $parts = explode(',', $field_handler);

    foreach ($parts as $name => $field) {

      $fields = $fields . " [" . trim($parts[$name]) . "] ";

    }

    return $fields;
  }

}

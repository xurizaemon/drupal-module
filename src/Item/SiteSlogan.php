<?php
/**
 * @file
 * Contains \Drupal\archimedes_client\Item\SiteSlogan
 */

namespace Drupal\archimedes_client\Item;

use Drupal\archimedes_client\Item;

/**
 * SiteSlogan
 *
 * The slogan or tagline of this site.
 *
 * @package Archimedes
 * @subpackage Client
 *
 */
class SiteSlogan extends Item
{
  /**
   * Gets the site slogan or tagline
   *
   * @return string
   *   Slogan
   */
  public function get()
  {
    $config = \Drupal::config('system.site');
    return $config->get('slogan');
  }
}

<?php
/**
 * @file
 * Contains \Drupal\archimedes_client\Item\DrupalVersion
 */

namespace Drupal\archimedes_client\Item;

use Drupal\archimedes_client\Item;
/**
 * DrupalVersion
 *
 * The current version of Drupal.
 *
 * @package Archimedes
 * @subpackage Client
 *
 */
class DrupalVersion extends Item
{
  /**
   * Gets the version string of Drupal
   *
   * @return string
   *   Version
   */
  public function get()
  {
    // Use the constant defined by Drupal
    return \Drupal::VERSION;
  }
}

<?php
/**
 * @file
 * Contains \Drupal\archimedes_client\Item\ServerName
 */

namespace Drupal\archimedes_client\Item;

use Drupal\Core\Url;
use Drupal\archimedes_client\Item;

/**
 * ServerName
 *
 * A URL that identifies this server.
 *
 * @package Archimedes
 * @subpackage Client
 *
 */
class ServerName extends Item
{
  /**
   * Gets the home URL of this Drupal site
   *
   * @return string
   *   URL
   */
  public function get()
  {
    // If Drush (or some other CLI passed method) is used to
    // run cron. You'll need to ensure a correct servername is
    // passed to PHP. With drush, use -l http://mysite.com.
    global $base_root;
    return $base_root;
  }
}

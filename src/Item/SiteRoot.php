<?php
/**
 * @file
 * Contains \Drupal\archimedes_client\Item\SiteRoot
 */

namespace Drupal\archimedes_client\Item;

use Drupal\archimedes_client\Item;
/**
 * SiteRoot
 *
 * Drupal's root directory on the server's file system.
 *
 * @package Archimedes
 * @subpackage Client
 *
 */
class SiteRoot extends Item
{
  /**
   * Gets the path to Drupal's root
   *
   * @return string
   *   Directory path
   */
  public function get()
  {
    // Use the constant defined by Drupal
    return DRUPAL_ROOT;
  }
}

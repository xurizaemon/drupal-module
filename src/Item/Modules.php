<?php

namespace Drupal\archimedes_client\Item;

use Drupal\archimedes_client\Item;

/**
 * Modules.
 *
 * Currently installed and enabled modules.
 *
 * @package Archimedes
 * @subpackage Client
 */
class Modules extends Item {

  /**
   * Gets an array of modules, keyed numerically.
   *
   * @return array
   *   Modules
   */
  public function get() {
    $module_handler = \Drupal::service('extension.list.module');

    $modules = [];
    foreach ($module_handler->reset()->getList() as $name => $module) {
      $info = $module->info;
      $modules[] = [
        'Module'      => $name,
        'Name'        => (isset($info['name']) ? $info['name'] : ''),
        'Description' => (isset($info['description']) ? $info['description'] : ''),
        'Package'     => (isset($info['package']) ? $info['package'] : ''),
        'Version'     => (isset($info['version']) ? $info['version'] : ''),
        'Core'        => (isset($info['core']) ? $info['core'] : ''),
      ];
    }
    return $modules;
  }

  /**
   * Gets a string denoting the number of modules installed.
   *
   * @return string
   *   HTML markup
   */
  public function render() {
    return "" . count($this->get()) . " modules";
  }

}

<?php
/**
 * @file
 * Contains \Drupal\archimedes_client\Controller\ArchimedesClientController.
 */

namespace Drupal\archimedes_client\Controller;

use Drupal\archimedes_client\Report;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;

/**
 * Controller routines for Archimedes Client routes.
 */
class ArchimedesClientController extends ControllerBase {

  /**
   * Returns the reporting status and data of the client.
   *
   * @return array
   *   A render array representing the administrative page content.
   */
  public function adminStatus() {
    $config = $this->config('archimedes_client.settings');

    // Get the reporting method
    $m = $config->get('server.method');
    switch ($m)
    {
      case 'http':
        $url = $config->get('server.url');
        $method = "HTTP - posted to $url";
        break;

      case 'email':
      default:
        $email = $config->get('server.email');
        $method = "Email - sent to $email";
        break;
    }

    // Get the report frequency
    $interval = $config->get('cron.interval');
    if ($interval > 86400)
    {
      $n = $interval / 86400;
      $freq = ($n == 1) ? 'day' : "$n days";
    }
    else if ($interval > 3600)
    {
      $n = $interval / 3600;
      $freq = ($n == 1) ? 'hour' : "$n hours";
    }
    else
    {
      $n = $interval;
      $freq = ($n == 1) ? 'second' : " $n seconds";
    }

    // Get the last report time
    $last_run = \Drupal::state()->get('archimedes_client.last_report', 0);
    $last_d = date('r', $last_run);
    $last = ($last_run == 0) ? 'No reports have been sent yet!' : "The last report was sent at $last_d";

    // Get the next report time
    $next_run = $last_run + $interval;
    $next_d = date('r', $next_run);
    $next = ($last_run == 0) ? 'as soon as possible' : "after $next_d";

    // Fetch rendered report items
    $report = new Report();
    $data = $report->getRendered();

    // Generate table items
    $rows = array();
    foreach ($data as $k => $v)
    {
      $rows[] = array( $k, array(
        'data' => $v,
        'id' => $k,
        'class' => 'report-field',
      ));
    }

    $table = array(
      '#type' => 'table',
      '#header' => array('Item', 'Value'),
      '#rows' => $rows,
      '#attributes' => array('id'=>'report'),
    );

    // Build the page HTML
    $html  = '';

    $html .= '<b>Report Method</b>';
    $html .= "<p>$method</p>";

    $html .= '<b>Report Frequency</b>';
    $html .= "<p>Reports sent every $freq</p>";

    $html .= '<b>Last Report</b>';
    $html .= "<p>$last</p>";

    $html .= '<b>Next Report</b>';
    $html .= "<p>Next report will be run $next</p>";

    $html .= '<b>Report Data</b>';
    $html .= drupal_render($table);

    $html .= "<p>View as ";
    $html .= \Drupal::l('Raw JSON', Url::fromRoute('archimedes_client.adminReport'));
    $html .= " or ";
    $html .= \Drupal::l('Pretty JSON', Url::fromRoute('archimedes_client.adminReport', array('pretty'=>1)));
    $html .= "</p>";

    $html .= "<p>";
    $html .= \Drupal::l('Change Archimedes Client settings...', Url::fromRoute('archimedes_client.adminSettings'));
    $html .= "</p>";

    $element = array(
      '#markup' => $html,
    );
    return $element;
  }

  /**
   * Returns a JSON encoded report
   *
   * @return array
   *   A render array representing the administrative page content.
   */
  public function adminReport() {

    // Create a report instance
    $report = new Report();

    // Set JSON mime type
    header('Content-Type: application/json');

    // Determine if pretty-print required or not, and output
    $pretty = isset($_GET['pretty']);
    echo $report->getJSON($pretty);

    exit;
  }
}
